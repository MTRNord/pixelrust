use axum::response::IntoResponse;
use thiserror::Error;

#[derive(Error, Debug)]
pub enum Error {
    #[error("Failed to parse URL: {0}")]
    UrlParseError(#[from] url::ParseError),
    #[error(transparent)]
    ActivityPubError(#[from] activitypub_federation::Error),
    #[error("Failed building instance: {0}")]
    InstanceSettingsBuilderError(#[from] activitypub_federation::InstanceSettingsBuilderError),
    #[error("I/O error: {0}")]
    IoError(#[from] std::io::Error),
    #[error("HTTP error: {0}")]
    HttpError(#[from] reqwest::Error),
    #[error("Failed to parse JSON: {0}")]
    JsonError(#[from] serde_json::Error),
    #[error("Failed to parse address: {0}")]
    AddrParseError(#[from] std::net::AddrParseError),
    #[error(transparent)]
    Other(#[from] anyhow::Error),
}

#[derive(Debug, Error)]
pub enum ServerError {
    #[error("Failed to convert to json: {0}")]
    JsonError(#[from] serde_json::Error),
    #[error("Failed to parse URL: {0}")]
    UrlParseError(#[from] url::ParseError),
    #[error(transparent)]
    InternalError(#[from] Error),
}

impl IntoResponse for ServerError {
    fn into_response(self) -> axum::response::Response {
        let error = match self {
            ServerError::UrlParseError(_) => String::from("Invalid user name"),
            ServerError::JsonError(_) => String::from("Failed to convert to json"),
            ServerError::InternalError(e) => e.to_string(),
        };
        (axum::http::StatusCode::INTERNAL_SERVER_ERROR, error).into_response()
    }
}
