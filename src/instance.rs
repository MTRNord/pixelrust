use std::{net::ToSocketAddrs, sync::Arc, time::Duration};

use activitypub_federation::{
    core::{
        axum::{inbox::receive_activity, json::ApubJson, verify_request_payload, DigestVerified},
        object_id::ObjectId,
        signatures::generate_actor_keypair,
    },
    data::Data,
    deser::context::WithContext,
    traits::ApubObject,
    InstanceSettings, LocalInstance,
};
use axum::{
    body,
    body::Body,
    extract::{OriginalUri, State},
    middleware,
    response::IntoResponse,
    routing::{get, post},
    Extension, Json, Router,
};
use hyper::{HeaderMap, Method, Request, StatusCode};
use reqwest::{Client, Url};
use tokio::sync::RwLock;
use tower::ServiceBuilder;
use tower_http::{trace::TraceLayer, ServiceBuilderExt};
use tracing::{debug, error, info};

use crate::{
    error::{Error, ServerError},
    generate_object_id,
    objects::{
        note::PixelfedPost,
        person::{Person, PersonAcceptedActivities, PixelfedUser},
    },
};

pub type InstanceHandle = Arc<Instance>;

pub struct Instance {
    /// This holds all library data
    local_instance: LocalInstance,
    /// Our "database" which contains all known users (local and federated)
    pub users: RwLock<Vec<PixelfedUser>>,
    /// Same, but for posts
    pub posts: RwLock<Vec<PixelfedPost>>,
}

impl Instance {
    pub fn new(hostname: String) -> Result<InstanceHandle, Error> {
        let settings = InstanceSettings::builder()
            .debug(true)
            //.url_verifier(Box::new(MyUrlVerifier()))
            .build()?;
        let client = Client::builder()
            .timeout(Duration::from_secs(30))
            .user_agent("Pixelrust")
            .build()?;
        let local_instance = LocalInstance::new(hostname.clone(), client.into(), settings);
        let id = generate_object_id(&hostname)?;
        let local_user = PixelfedUser::new(
            id.to_string()
                .replace(&format!("http://{}/users/", hostname), ""),
            id,
            generate_actor_keypair()?,
        );
        let instance = Arc::new(Instance {
            local_instance,
            users: RwLock::new(vec![local_user]),
            posts: RwLock::new(vec![]),
        });
        Ok(instance)
    }

    pub async fn local_user(&self) -> Result<PixelfedUser, Error> {
        Ok(self
            .users
            .read()
            .await
            .first()
            .cloned()
            .expect("No local user found"))
    }

    pub fn local_instance(&self) -> &LocalInstance {
        &self.local_instance
    }

    pub async fn listen(instance: &InstanceHandle) -> Result<(), Error> {
        let hostname = instance.local_instance.hostname();
        let instance = instance.clone();

        let app = Router::new()
            .route("/users/:user_name/inbox", post(http_post_user_inbox))
            .layer(
                ServiceBuilder::new()
                    .map_request_body(body::boxed)
                    .layer(middleware::from_fn(verify_request_payload)),
            )
            .route("/users/:user_name", get(get_user))
            .with_state(instance)
            .layer(TraceLayer::new_for_http());
        let addr = hostname
            .to_socket_addrs()?
            .next()
            .expect("Failed to lookup domain name");
        info!("listening on {}", addr);
        axum::Server::bind(&addr)
            .serve(app.into_make_service())
            .await
            .unwrap();
        Ok(())
    }
}

async fn get_user(
    State(instance): State<InstanceHandle>,
    request: Request<Body>,
) -> Result<ApubJson<WithContext<Person>>, ServerError> {
    let hostname: String = instance.local_instance.hostname().to_string();
    let request_url = format!("http://{}{}", hostname, &request.uri());
    debug!("User was requested: {}", request.uri());

    let url = Url::parse(&request_url)?;

    let user = ObjectId::<PixelfedUser>::new(url)
        .dereference_local(&instance)
        .await
        .expect("Failed to dereference user")
        .into_apub(&instance)
        .await
        .expect("Failed to convert to apub user");

    Ok(ApubJson(WithContext::new_default(user)))
}

/// Handles messages received in user inbox
async fn http_post_user_inbox(
    headers: HeaderMap,
    method: Method,
    OriginalUri(uri): OriginalUri,
    State(data): State<InstanceHandle>,
    Extension(digest_verified): Extension<DigestVerified>,
    Json(activity): Json<WithContext<PersonAcceptedActivities>>,
) -> impl IntoResponse {
    debug!("User got inbox content");
    match receive_activity::<WithContext<PersonAcceptedActivities>, PixelfedUser, InstanceHandle>(
        digest_verified,
        activity,
        &data.clone().local_instance,
        &Data::new(data),
        headers,
        method,
        uri,
    )
    .await
    {
        Ok(_) => StatusCode::OK,
        Err(e) => {
            error!("Failed to accept inbox content: {e:?}");
            StatusCode::INTERNAL_SERVER_ERROR
        }
    }
}
