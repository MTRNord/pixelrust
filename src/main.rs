use std::sync::Arc;

use color_eyre::eyre::Result;
use pixelrust::{instance::Instance, objects::note::PixelfedPost};
use tracing::info;

#[actix_rt::main]
async fn main() -> Result<()> {
    color_eyre::install()?;
    tracing_subscriber::fmt::init();

    let alpha = Instance::new("localhost:8001".to_string())?;
    let alpha2 = Arc::clone(&alpha);
    tokio::spawn(async move { Instance::listen(&Arc::clone(&alpha2)).await });

    let beta = Instance::new("localhost:8002".to_string())?;
    let beta2 = Arc::clone(&beta);
    tokio::spawn(async move { Instance::listen(&Arc::clone(&beta2)).await });

    // alpha user follows beta user
    info!("Alpha going to follow beta user");
    alpha
        .local_user()
        .await?
        .follow(&beta.local_user().await?, &alpha)
        .await?;

    // assert that follow worked correctly
    assert_eq!(
        beta.local_user().await?.followers(),
        &vec![alpha.local_user().await?.ap_id.inner().clone()]
    );
    info!(
        "Alpha follows beta: {:?}",
        beta.local_user().await?.followers()
    );

    // beta sends a post to its followers
    info!("Beta is going to send a messagge to followers");
    let sent_post = PixelfedPost::new("hello world!".to_string(), beta.local_user().await?.ap_id);
    beta.local_user()
        .await?
        .post(sent_post.clone(), &beta)
        .await?;
    let received_post = alpha.posts.read().await.first().cloned().unwrap();

    // assert that alpha received the post
    assert_eq!(received_post.text, sent_post.text);
    assert_eq!(received_post.ap_id.inner(), sent_post.ap_id.inner());
    assert_eq!(received_post.creator.inner(), sent_post.creator.inner());
    info!(
        "Alpha received post: {:?}\n{:?}\n{:?}",
        received_post.text,
        received_post.ap_id.inner(),
        received_post.creator.inner()
    );
    Ok(())
}
